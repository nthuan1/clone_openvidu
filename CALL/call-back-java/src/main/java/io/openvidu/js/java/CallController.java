package io.openvidu.js.java;

import io.openvidu.java.client.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("")
public class CallController {

    // OpenVidu object as entrypoint of the SDK
    private OpenVidu openVidu;

    // Collection to pair session names and OpenVidu Session objects
    private Map<String, Session> mapSessions = new ConcurrentHashMap<>();
    // Collection to pair session names and tokens (the inner Map pairs tokens and
    // role associated)
    private Map<String, Map<String, OpenViduRole>> mapSessionNamesTokens = new ConcurrentHashMap<>();

    // URL where our OpenVidu server is listening
    private String OPENVIDU_URL;
    // Secret shared with our OpenVidu server
    private String SECRET;

    public CallController(@Value("${openvidu.secret}") String secret, @Value("${openvidu.url}") String openviduUrl) {
        this.SECRET = secret;
        this.OPENVIDU_URL = openviduUrl;
        this.openVidu = new OpenVidu(OPENVIDU_URL, SECRET);
    }

    @RequestMapping(value = "/call", method = RequestMethod.POST)
    public ResponseEntity<String> getToken(@RequestBody String sessionId)
            throws ParseException {
//
//		try {
//			checkUserLogged(httpSession);
//		} catch (Exception e) {
//			return getErrorResponse(e);
//		}
        System.out.println("Getting a token from OpenVidu Server | {sessionName}=" + sessionId);

        JSONObject sessionJSON = (JSONObject) new JSONParser().parse(sessionId);

        // The video-call to connect
        String sessionName = (String) sessionJSON.get("sessionId");

        ConnectionProperties connectionProperties = new ConnectionProperties.Builder()
                .type(ConnectionType.WEBRTC)
                .role(OpenViduRole.PUBLISHER)
                .build();

//		JSONObject responseJson = new JSONObject();


        // New session
        System.out.println("New session " + sessionName);
        try {

            // Create a new OpenVidu Session
            SessionProperties properties = new SessionProperties.Builder()
                    .customSessionId(sessionName)
                    .build();
            Session session = this.openVidu.createSession(properties);
            // Generate a new Connection with the recently created connectionProperties
            String token = session.createConnection(connectionProperties).getToken();


            // Return the response to the client
            return new ResponseEntity<>("\"" + token + "\"", HttpStatus.OK);

        } catch (Exception e) {
            // If error generate an error message and return it to client
            return getErrorResponse(e);
        }
    }

    private ResponseEntity<String> getErrorResponse(Exception e) {
        JSONObject json = new JSONObject();
        json.put("cause", e.getCause());
        json.put("error", e.getMessage());
        json.put("exception", e.getClass());
        return new ResponseEntity<>(json.toJSONString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}