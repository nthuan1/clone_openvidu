export const SERVER_PORT = process.env.SERVER_PORT || 5000;
export const OPENVIDU_URL = process.env.OPENVIDU_URL || 'https://202.134.19.107:4443';
export const OPENVIDU_SECRET = process.env.OPENVIDU_SECRET || '123456';
export const CALL_OPENVIDU_CERTTYPE = process.env.CALL_OPENVIDU_CERTTYPE;
